const server = require('./src/server_config');
const conn = require('./src/data_config').connection;

server.listen(3000)

server.post('/login', (req,res) => {	
	conn.getConnection((err,connection) => {
		conn.query('select * from UsuarioLogin where email_usuario = ? and senha_usuario = ?',[req.body.email,req.body.senha],function(err,result,fields){
			connection.release();
			if(err){
				res.sendStatus(500);
				return;
			}
			if(result[0]){
				res.sendStatus(200);
				return;
			} else {
				res.send(403);
				return;
			}
				
		})
	})
})

server.post('/cadastrar', (req,res) => {
	var data = {
		email: req.body.email,
		senha: req.body.senha
	}
	var msgDuplicate = "Duplicate entry '" + data.email + "' for key 'email_usuario_UNIQUE'";
	conn.getConnection((err,connection) => {
		conn.query('insert into UsuarioLogin (email_usuario,senha_usuario) values (?,?)', [data.email,data.senha], (error,result,fields) => {
			connection.release();
			if(error){
				if (error.sqlMessage == msgDuplicate){
					res.sendStatus(422);
					return;
				}
				res.sendStatus(500);
				return;
			}
			res.sendStatus(201);
			return;
		})
	})
})