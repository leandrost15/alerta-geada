<h1>INTEGRANTES ALERTA DE GEADA</h1>

• [GABRIEL DOS SANTOS SILVA](https://gitlab.com/leandrost15/alerta-geada/wikis/Gabriel-dos-Santos-Silva)
    
• [GABRIEL NUNES HENKE](https://gitlab.com/leandrost15/alerta-geada/wikis/Gabriel-Nunes-Henke)
    
• [GUILHERME BARBOSA BERALDO](https://gitlab.com/leandrost15/alerta-geada/wikis/Guilherme-Barbosa-Beraldo)
    
• [JOÃO VITOR FALÉCO](https://gitlab.com/leandrost15/alerta-geada/wikis/Jo%C3%A3o-Vitor-Fal%C3%A9co)
    
• [LEANDRO SALUSTRIANO DOS SANTOS](https://gitlab.com/leandrost15/alerta-geada/wikis/Leandro-Salustriano-dos-Santos)
    
• [PEDRO PRATES MARIANO SANT’ANNA DE OLIVEIRA](https://gitlab.com/leandrost15/alerta-geada/wikis/Pedro-Prates-Mariano-Sant'anna-de-Oliveira)
    
• [RAFAEL RODOLFO PINTO CHAVES](https://gitlab.com/leandrost15/alerta-geada/wikis/Rafael)
    
• [VICTOR RIBEIRO DE CAMARGO MACHADO](https://gitlab.com/leandrost15/alerta-geada/wikis/Victor-Ribeiro-de-Camargo-Machado)
    